package com.personal.loganmckinzie.wordacity;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;


public class GameFragment extends Fragment {

    GameInterface gameInterface;

    String[] charactersDisplay = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    String[] characters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    ArrayList<Card> cards = new ArrayList<>();

    //Game Variables
    int totalGridPointValue;
    int selectedPoints;
    int goal;
    int goalProgress;
    int timer;
    int score;
    int lives;
    boolean timerFinished;
    ArrayList<String> usedWords = new ArrayList<>();

    TextView wordSpace;

    String selectedLetters;

    public GameFragment() {
        // Required empty public constructor
    }

    public static GameFragment newInstance(int score, int lives) {

        Bundle bundle = new Bundle();
        bundle.putInt("SCORE", score);
        bundle.putInt("LIVES", lives);

        GameFragment gameFragment = new GameFragment();
        gameFragment.setArguments(bundle);

        return gameFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        gameInterface = (GameInterface) activity;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        createLetterGrid();

        score = getArguments().getInt("SCORE");
        lives = getArguments().getInt("LIVES");

        goalProgress = 0;
        updateUI(StaticVariables.GOAL_PROGRESS_EXTRA);

        goal = totalGridPointValue * 2;
        updateUI(StaticVariables.GOAL_EXTRA);

        selectedLetters = "";

        usedWords.add("");

        wordSpace = (TextView) getActivity().findViewById(R.id.wordSpace);

        FrameLayout wordacityLayout = (FrameLayout) getActivity().findViewById(R.id.wordacity);
        ImageButton cancelButton = (ImageButton) getActivity().findViewById(R.id.cancelButton);

        timerFinished = true;
        updateUI(StaticVariables.TIMER_EXTRA);

        wordacityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedLetters.equals("")) {
                    // TODO: WHAT IF NOTHING IS SELECTED?
                } else {
                    wordacity(selectedLetters);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelectedLetters();
            }
        });

        for (Card card : cards) {
            final Card currentCard = card;
            currentCard.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentCard.getLayout().getAlpha() != (float) 0.5) {
                        currentCard.getPointValue();
                        currentCard.getLayout().setAlpha((float) 0.5);
                        selectedPoints += currentCard.getPointValue();
                        addLetter(currentCard.getLetter());
                    }
                }
            });
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        gameInterface = null;
    }

    public void endGame(Context context, int score) {
        SaveHelper.saveData(score, context);

        Intent intent = new Intent(context, LeaderboardActivity.class);

        context.startActivity(intent);
    }

    public void updateUI(String extraName) {

        if (extraName.equals(StaticVariables.TIMER_EXTRA)) {
            gameInterface.updateTimer(timerFinished);
        } else if (extraName.equals(StaticVariables.SCORE_EXTRA)) {
            gameInterface.updateScore(score);
        } else if (extraName.equals(StaticVariables.GOAL_EXTRA)) {
            gameInterface.updateGoal(goal);
        } else if (extraName.equals(StaticVariables.GOAL_PROGRESS_EXTRA)) {
            gameInterface.updateGoalProgress(goalProgress);
        } else {
            gameInterface.updateLives(lives);
        }

    }

    public void addLetter(String letter) {
        selectedLetters += letter;
        wordSpace.setText(selectedLetters);
        wordSpace.setAllCaps(true);
    }

    public void resetSelectedLetters() {
        for (Card card : cards) {
            card.getLayout().setAlpha(1);
        }

        selectedLetters = "";
        selectedPoints = 0;

        wordSpace.setText("");
        wordSpace.setTextColor(Color.BLACK);
    }

    public void wordacity(String wordacityWord) {
        String[] dictionary = getResources().getStringArray(R.array.words);

        boolean wordCorrect = false;
        boolean wordUsed = false;

        for (String word : dictionary) {
            if (wordacityWord.equals(word)) {
                for (String word2 : usedWords) {
                    if (wordacityWord.equals(word2)) {
                        wordUsed = true;
                        break;
                    }
                }

                if (!wordUsed) {
                    wordSpace.setTextColor(Color.GREEN);
                    wordCorrect = true;
                    score += selectedPoints;
                    goalProgress += selectedPoints;
                    usedWords.add(wordacityWord);

                    updateUI(StaticVariables.GOAL_PROGRESS_EXTRA);
                    updateUI(StaticVariables.SCORE_EXTRA);

                    if (goalProgress >= goal) {
                        timerFinished = false;
                        updateUI(StaticVariables.TIMER_EXTRA);
                    }

                    break;
                }
            }

            if (wordCorrect || wordUsed) {
                break;
            }
        }

        if (!wordCorrect && !wordUsed) {
            lives--;
            updateUI(StaticVariables.LIVES_EXTRA);
            wordSpace.setTextColor(Color.RED);
        } else if (!wordCorrect && wordUsed) {
            wordSpace.setTextColor(Color.YELLOW);
        }

        CountDownTimer timer = new CountDownTimer(300, 300) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                resetSelectedLetters();

                if (lives <= 0) {
                    endGame(getActivity(), score);
                }
            }
        };

        timer.start();
    }

    public void createLetterGrid() {
        TextView[] letters = {
                (TextView) getActivity().findViewById(R.id.square1),
                (TextView) getActivity().findViewById(R.id.square2),
                (TextView) getActivity().findViewById(R.id.square3),
                (TextView) getActivity().findViewById(R.id.square4),
                (TextView) getActivity().findViewById(R.id.square5),
                (TextView) getActivity().findViewById(R.id.square6),
                (TextView) getActivity().findViewById(R.id.square7),
                (TextView) getActivity().findViewById(R.id.square8),
                (TextView) getActivity().findViewById(R.id.square9),
                (TextView) getActivity().findViewById(R.id.square10),
                (TextView) getActivity().findViewById(R.id.square11),
                (TextView) getActivity().findViewById(R.id.square12),
                (TextView) getActivity().findViewById(R.id.square13),
                (TextView) getActivity().findViewById(R.id.square14),
                (TextView) getActivity().findViewById(R.id.square15),
                (TextView) getActivity().findViewById(R.id.square16)
        };

        TextView[] points = {
                (TextView) getActivity().findViewById(R.id.squarePoint1),
                (TextView) getActivity().findViewById(R.id.squarePoint2),
                (TextView) getActivity().findViewById(R.id.squarePoint3),
                (TextView) getActivity().findViewById(R.id.squarePoint4),
                (TextView) getActivity().findViewById(R.id.squarePoint5),
                (TextView) getActivity().findViewById(R.id.squarePoint6),
                (TextView) getActivity().findViewById(R.id.squarePoint7),
                (TextView) getActivity().findViewById(R.id.squarePoint8),
                (TextView) getActivity().findViewById(R.id.squarePoint9),
                (TextView) getActivity().findViewById(R.id.squarePoint10),
                (TextView) getActivity().findViewById(R.id.squarePoint11),
                (TextView) getActivity().findViewById(R.id.squarePoint12),
                (TextView) getActivity().findViewById(R.id.squarePoint13),
                (TextView) getActivity().findViewById(R.id.squarePoint14),
                (TextView) getActivity().findViewById(R.id.squarePoint15),
                (TextView) getActivity().findViewById(R.id.squarePoint16)
        };

        FrameLayout[] cardLayouts = {
                (FrameLayout) getActivity().findViewById(R.id.card1),
                (FrameLayout) getActivity().findViewById(R.id.card2),
                (FrameLayout) getActivity().findViewById(R.id.card3),
                (FrameLayout) getActivity().findViewById(R.id.card4),
                (FrameLayout) getActivity().findViewById(R.id.card5),
                (FrameLayout) getActivity().findViewById(R.id.card6),
                (FrameLayout) getActivity().findViewById(R.id.card7),
                (FrameLayout) getActivity().findViewById(R.id.card8),
                (FrameLayout) getActivity().findViewById(R.id.card9),
                (FrameLayout) getActivity().findViewById(R.id.card10),
                (FrameLayout) getActivity().findViewById(R.id.card11),
                (FrameLayout) getActivity().findViewById(R.id.card12),
                (FrameLayout) getActivity().findViewById(R.id.card13),
                (FrameLayout) getActivity().findViewById(R.id.card14),
                (FrameLayout) getActivity().findViewById(R.id.card15),
                (FrameLayout) getActivity().findViewById(R.id.card16)
        };

        String[] pointValues = {"1", "3", "3", "2", "1", "4", "2", "4", "1", "8", "5", "1", "3", "1", "1", "3", "10", "1", "1", "1", "1", "4", "4", "8", "4", "10"};

        for (int i = 0; i < letters.length; i++) {

            String newLetter = letterRandomizer();

            letters[i].setText(newLetter);

            Card card = new Card();
            card.setLayout(cardLayouts[i]);

            for (int k = 0; k < characters.length; k++) {
                if (charactersDisplay[k].equals(newLetter)) {
                    points[i].setText(pointValues[k]);
                    card.setLetter(characters[k]);
                    card.setPointValue(Integer.valueOf(pointValues[k]));
                    cards.add(card);
                    totalGridPointValue += card.getPointValue();
                }
            }
        }
    }

    public String letterRandomizer() {

        double a = 0.07;
        double b = 0.01;
        double c = 0.03;
        double d = 0.04;
        double e = 0.11;
        double f = 0.02;
        double g = 0.02;
        double h = 0.06;
        double i2 = 0.06;
        double j = 0.01;
        double k = 0.01;
        double l = 0.04;
        double m = 0.02;
        double n = 0.07;
        double o = 0.07;
        double p = 0.02;
        double q = 0.01;
        double r = 0.06;
        double s = 0.06;
        double t = 0.09;
        double u = 0.03;
        double v = 0.01;
        double w = 0.02;
        double x = 0.01;
        double y = 0.02;
        double z = 0.01;

        double[] weights = {a, b, c, d, e, f, g, h, i2, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z};

        // Compute the total weight of all items together
        double totalWeight = 0.0d;

        for (double i : weights) {
            totalWeight += i;
        }

        // Now choose a random item
        int randomIndex = -1;
        double random = Math.random() * totalWeight;

        for (int i = 0; i < weights.length; ++i) {

            random -= weights[i];

            if (random <= 0.0d) {
                randomIndex = i;
                break;
            }
        }

        return charactersDisplay[randomIndex];
    }

    public interface GameInterface {
        void updateTimer(boolean timerFinished);
        void updateScore(int score);
        void updateGoal(int goal);
        void updateGoalProgress(int goalProgress);
        void updateLives(int lives);
    }
}
