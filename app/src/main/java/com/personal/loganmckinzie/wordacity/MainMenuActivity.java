package com.personal.loganmckinzie.wordacity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//import com.firebase.client.AuthData;
//import com.firebase.client.Firebase;


public class MainMenuActivity extends Activity implements View.OnClickListener {

    android.content.Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        mContext = this;

        Button buttonStartGame = (Button) findViewById(R.id.buttonStartGame);
        buttonStartGame.setOnClickListener(this);

        Button buttonLeaderboards = (Button) findViewById(R.id.buttonLeaderboards);
        buttonLeaderboards.setOnClickListener(this);

        Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);

        Button buttonAbout = (Button) findViewById(R.id.buttonAbout);
        buttonAbout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonStartGame) {
            startGame();

        } else if (v.getId() == R.id.buttonLeaderboards) {
            viewLeaderboard();

        } else if (v.getId() == R.id.buttonLogin) {
            new AlertDialog.Builder(this)
                    .setTitle("Change Account")
                    .setMessage("Would you like to change users, or register a new account?")
                    .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            accountChange();
                        }
                    })
                    .setNegativeButton("Register", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            registerNewUser();
                        }
                    })
                    .show();

        } else {
            openAbout();
        }
    }

    // Starts the game
    public void startGame() {
//        final Firebase ref = new Firebase("https://wordacity.firebaseIO.com");
//
//        AuthData authData = ref.getAuth();
//
//        if (authData != null) {
//            startActivity(new Intent(mContext, GameActivity.class));
//        } else {
//            AlertDialog alertLogin = new AlertDialog.Builder(mContext)
//                    .setTitle("Warning")
//                    .setMessage("If you don't login, your scores will not be saved. Are you sure?")
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            startActivity(new Intent(mContext, GameActivity.class));
//                        }
//                    })
//                    .setNegativeButton("No", null)
//                    .show();
//        }
        startActivity(new Intent(this, GameActivity.class));
    }

    // Opens the leaderboard
    public void viewLeaderboard() {
        startActivity(new Intent(this, LeaderboardActivity.class));
    }

    // Triggered when the user wants to register a new account
    public void registerNewUser() {
        startActivity(new Intent(this, LoginActivity.class).putExtra(StaticVariables.NEW_ACCOUNT, true));
    }

    // Triggered when the user wants to change their account
    public void accountChange() {
        startActivity(new Intent(this, LoginActivity.class).putExtra(StaticVariables.NEW_ACCOUNT, false));
    }

    // Opens about dialog
    public void openAbout() {
        new AlertDialog.Builder(this)
                .setTitle("About")
                .setMessage("Developed by Logan McKinzie")
                .setPositiveButton("Dismiss", null)
                .show();
    }
}
