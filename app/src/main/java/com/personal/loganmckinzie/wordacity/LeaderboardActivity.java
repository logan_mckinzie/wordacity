package com.personal.loganmckinzie.wordacity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class LeaderboardActivity extends Activity {

    Context mContext;
    ArrayList<TextView> scoreTexts;

    TextView userScore;
    TextView playerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        setupUI();
    }

    private void setupUI() {
        mContext = this;

        ImageButton deleteScoreButton = (ImageButton) findViewById(R.id.deleteScoreButton);
        deleteScoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete Score")
                        .setMessage("Are you sure you want to delete your high score?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SaveHelper.deleteData(mContext);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        userScore = (TextView) findViewById(R.id.playerScore);
        playerName = (TextView) findViewById(R.id.playerName);

        userScore.setText(String.valueOf(SaveHelper.getData(this)));
    }
}
