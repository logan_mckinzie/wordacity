package com.personal.loganmckinzie.wordacity;

import android.widget.FrameLayout;

/**
 * Created by loganmckinzie on 4/20/16.
 */
public class Card {

    FrameLayout layout;
    String letter;
    int pointValue;

    public Card() {
    }

    public Card(FrameLayout layout, String letter, int pointValue) {
        this.layout = layout;
        this.letter = letter;
        this.pointValue = pointValue;
    }

    public FrameLayout getLayout() {
        return layout;
    }

    public void setLayout(FrameLayout layout) {
        this.layout = layout;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getPointValue() {
        return pointValue;
    }

    public void setPointValue(int pointValue) {
        this.pointValue = pointValue;
    }
}
