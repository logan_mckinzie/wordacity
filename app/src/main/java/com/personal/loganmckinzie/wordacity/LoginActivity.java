package com.personal.loganmckinzie.wordacity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import com.firebase.client.Firebase;
//import com.firebase.client.FirebaseError;

import java.util.Map;

public class LoginActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button confirmAccount = (Button) findViewById(R.id.buttonConfirmAccount);
        confirmAccount.setOnClickListener(this);

        if (getIntent().getBooleanExtra(StaticVariables.NEW_ACCOUNT, false)) {
            confirmAccount.setText("Register");
        }
    }

    @Override
    public void onClick(View v) {
        if (getIntent().getBooleanExtra(StaticVariables.NEW_ACCOUNT, false)) {
            createAccount(true);
        } else {
            createAccount(false);
        }
    }

    public void createAccount(boolean newAccount) {
          if (newAccount) {
//            Firebase.setAndroidContext(this);
//            final Firebase ref = new Firebase("https://wordacity.firebaseIO.com/username");
//
//            EditText username = (EditText) findViewById(R.id.editTextUsername);
//            EditText email = (EditText) findViewById(R.id.editTextEmail);
//            EditText password = (EditText) findViewById(R.id.editTextPassword);
//
//            final String usernameString = (username.getText().toString());
//
//            ref.createUser(email.getText().toString(), password.getText().toString(), new Firebase.ValueResultHandler<Map<String, Object>>() {
//                @Override
//                public void onSuccess(Map<String, Object> result) {
//                    System.out.println("Successfully created user account with uid: " + result.get("uid"));
//                    ref.setValue(usernameString);
//                    finish();
//                }
//
//                @Override
//                public void onError(FirebaseError firebaseError) {
//                    Toast.makeText(LoginActivity.this, firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            });
        } else {
            // TODO: LOGGING IN
            finish();
        }

    }
}
