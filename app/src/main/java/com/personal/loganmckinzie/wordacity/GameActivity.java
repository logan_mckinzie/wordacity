package com.personal.loganmckinzie.wordacity;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class GameActivity extends AppCompatActivity implements GameFragment.GameInterface {

    int goal;
    int goalProgress;
    CountDownTimer timer;
    int score;
    int lives;

    TextView timerText;
    TextView scoreText;
    TextView goalText;
    TextView livesText;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        goal = 0;
        goalProgress = 0;
        score = 0;
        lives = 3;

        mContext = this;

        GameFragment frag = GameFragment.newInstance(score, lives);
        getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, frag, null).commit();

        timerText = (TextView) findViewById(R.id.timerText);
        scoreText = (TextView) findViewById(R.id.scoreText);
        goalText = (TextView) findViewById(R.id.goalText);
        livesText = (TextView) findViewById(R.id.livesText);

        timer = new CountDownTimer(100000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerText.setText(String.valueOf((int) TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)));
            }

            @Override
            public void onFinish() {
                endGame(mContext, score);
            }
        };
    }

    public void endGame(Context context, int score) {
        SaveHelper.saveData(score, context);

        Intent intent = new Intent(context, LeaderboardActivity.class);

        context.startActivity(intent);
    }

    @Override
    public void updateTimer(boolean timerFinished) {
        if (timerFinished) {
            timer.start();
        } else {
            GameFragment frag = GameFragment.newInstance(score, lives);

            getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, frag, null).commit();
        }
    }

    @Override
    public void updateScore(int score) {
        this.score =+ score;
        scoreText.setText(String.valueOf(score));
    }

    @Override
    public void updateGoal(int goal) {
        this.goal = goal;
        String goalTextInput = goalProgress + "/" + goal;
        goalText.setText(goalTextInput);
    }

    @Override
    public void updateGoalProgress(int goalProgress) {
        this.goalProgress = goalProgress;
        String goalTextInput = goalProgress + "/" + goal;
        goalText.setText(goalTextInput);
    }

    @Override
    public void updateLives(int lives) {
        this.lives = lives;
        livesText.setText(String.valueOf(lives));
    }
}
