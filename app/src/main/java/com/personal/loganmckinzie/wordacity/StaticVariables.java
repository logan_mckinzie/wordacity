package com.personal.loganmckinzie.wordacity;

/**
 * Created by loganmckinzie on 4/18/16.
 */
public class StaticVariables {

    public static String NEW_ACCOUNT = "NEW_ACCOUNT";

    public static String TIMER_EXTRA = "TIMER_EXTRA";
    public static String SCORE_EXTRA = "SCORE_EXTRA";
    public static String GOAL_EXTRA = "GOAL_EXTRA";
    public static String GOAL_PROGRESS_EXTRA = "GOAL_PROGRESS_EXTRA";
    public static String LIVES_EXTRA = "LIVES_EXTRA";

    public static String GAME_UPDATE = "GAME_UPDATE";
}
