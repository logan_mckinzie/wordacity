package com.personal.loganmckinzie.wordacity;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by loganmckinzie on 4/23/16.
 */
public class SaveHelper {

    public static void saveData(int newScore, Context mContext) {

        try {
            File file = new File(mContext.getFilesDir()+"/"+"score");

            if (!file.exists()) {
                file = new File(mContext.getFilesDir()+"/"+"score");
            }

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(newScore);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getData(Context mContext) {

        int score = 0;

        try {
            File file = new File(mContext.getFilesDir()+"/"+"score");

            if (!file.exists()) {
                file = new File(mContext.getFilesDir()+"/"+"score");
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            score = (int) ois.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return score;
    }

    public static void deleteData(Context mContext) {

        int score = 0;

        try {
            File file = new File(mContext.getFilesDir()+"/"+"score");

            if (!file.exists()) {
                file = new File(mContext.getFilesDir()+"/"+"score");
            }

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(score);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

